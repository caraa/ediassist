Pour exécuter notre programme, il suffit d'utiliser n'importe quel environnement de développement disposant de la troisième
version de Python, que ce soit sur Windows ou Linux.

Néanmoins, il est important d'installer les bibliothèques suivantes :
-tkinter
-customtkiter (pour les interfaces graphiques)
-os (pour la lecture / écriture dans un fichier)
-json
-requests (pour le fonctionnement de l'intelligence artificielle)

Enfin, l'ordinateur utilisé doit impérativement être CONNECTE A INTERNET pour que l'intelligence artificielle fonctionne.