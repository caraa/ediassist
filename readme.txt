Pour correctement utiliser notre petit logiciel, il est impératif de respecter ces différentes étapes :

- Exécuter le programme principal GUI_EDDIASSIST.py dans n'importe quel environnement de développement 
disposant de la troisième version de Python.

- Une fois que la fenêtre d'accueil est entièrement apparue, cliquer sur le bouton "commencer".

- Une fois dans le menu principal, cliquer sur les boutons "qcm" ou "flashcards" en fonction du choix.

- Dans les menus de paramétrage, indiquer les noms de la matière, et du thème travaillé : cela est relatif 
aux noms des dossiers et fichiers.
Attention, il est très important d'écrire les noms des dossiers / fichiers avec les mots séparés par des tirets du bas (_).

- Ensuite, le protocole diffère entre la partie qcm, et la partie flashcards... 

A = qcm, il y a quelques manipulations supplémentaires à effectuer (cause manque de temps dans notre projet).
B = Flashcards.

A.1) Pour la partie qcm, il suffit ensuite d'indiquer le nombre de questions et de réponses souhaité, puis de 
coller le texte / le cours dans la partie dédiée à cet effet. Cliquer par la suite sur "lancer", et attendre quelques secondes.
A.2) Un fichier info_qcm.py a été créé, il suffit ensuite de couper la variable liste_qcm ainsi que tout son contenu 
(toute la liste de dictionnaires).
A.3) Après, il est nécessaire de coller la variable et on contenu dans le fichier nommé test_qcm_final.py 
(à la ligne 65 environ, à l'endroit indiquer par un commentaire).
A.4) Enfin, il faut exécuter le programme test_qcm_final.py et les questions / réponses apparaissent.
A.5) Il suffit ensuite de répondre aux questions en cliquant sur le bouton correspondant à la réponse choisie.
A.6) Attention à bien effacer du programme ce qui a été coller pour pouvoir réutiliser le programme...

B.1) Pour la partie flashcards, cela est beaucoup plus simple : il suffit d'indiquer le nombre de questions souhaité, puis de 
coller le texte / le cours dans la partie dédiée à cet effet. Cliquer par la suite sur "lancer".
B.2) Il suffit ensuite de répondre aux questions qui apparaissent, en cliquant sur "afficher réponse" pour enfin 
découvrir la réponse à la question posée.