#GUI pour notre petit logiciel
from tkinter import *
from customtkinter import * #Importation de la bibliothèque graphique tkinter, avec une autre bibliothèque customtkiter qui permet des GUI plus modernes
import os
import ia



#Nous avons fait le choix de programmation de définir chaque nouvelle fenêtre dès le départ (programme principal), pour ensuite cacher immédiatement les fenêtres secondaires, inutiles au départ.
#Ensuite, l'accès aux fenêtres se faits grâce aux boutons qui ont comme commande des callback des fonctions qui contiennenent les instructiosns pour cacher les fenêtres non voulues et de découvrir la fenêtre voulu



def creer_fenetre_choix(): #//Définition de la fenêtre de choix en QCM et Flashcards (menu) dans une fonction pour pouvoir effectuer un callback avec les bouttons
    """
    permet de creer la fenetre choix
    """


    fenetre_choix.deiconify() #On découvre la fenêtre voulue

    fenetre_accueil.withdraw() #On cache toujours bel et bien les autres


def creer_fenetre_para_qcm():
    """
    permet de creer creer_fenetre_para_qcm
    """

    fenetre_para_qcm.deiconify() #On découvre la fenêtre voulue

    fenetre_choix.withdraw() #On cache toujours bel et bien les autres


def creer_fenetre_para_flash():
    """
    permet de creer creer_fenetre_para_qcm
    :return:
    """

    fenetre_para_flash.deiconify() #On découvre la fenêtre voulue

    fenetre_choix.withdraw() #On cache toujours bel et bien les autres


def creer_fenetre_qcm_plus_get_info_explo(): #//Définition de la fenêtre paramétrage QCM dans une fonction pour pouvoir effectuer un callback avec les bouttons
    """
    permet de creer_fenetre_qcm et des creer le dossier avec la matiere
    """

    fenetre_qcm.deiconify() #On découvre la fenêtre voulue

    fenetre_para_qcm.withdraw() #On cache toujours bel et bien les autres

    try:
        os.mkdir(txt_box_dossier_qcm.get("1.0", "end-1c"))
    except:
        pass
    acces = txt_box_dossier_qcm.get("1.0", "end-1c")+"\ "+txt_box_fichier_qcm.get("1.0", "end-1c")
    info = open(acces + ".py", "a")
    info.close()


def creer_fenetre_flash_plus_get_info_explo(): #//Définition de la fenêtre paramétrage des flashcards dans une fonction pour pouvoir effectuer un callback avec les bouttons
    """
    permet de creer_fenetre_flashcards et des creer le dossier avec la matiere
    """

    fenetre_flash.deiconify() #On découvre la fenêtre voulue

    fenetre_para_flash.withdraw() #On cache toujours bel et bien les autres
    try:
        os.mkdir(txt_box_dossier_flash.get("1.0", "end-1c"))
    except:
        pass
def get_info_qcm():
    """
    fonction permetant de prendre de recuperer l'accès et l'envoyer dans l'ia
    :return:
    """
    acces = txt_box_dossier_qcm.get("1.0", "end-1c") + "\ " + txt_box_fichier_qcm.get("1.0", "end-1c")
    nb_q_qcm = txt_box_nb_q_qcm.get("1.0", "end-1c")
    text = txt_box_ai_qcm.get("1.0", "end-1c")
    ia.mode_ia_qcm(nb_q_qcm, text,acces)

def get_info_flash():
    """
    fonction permetant de prendre de recuperer l'accès et l'envoyer dans l'ia
    :return:
    """
    acces = txt_box_dossier_flash.get("1.0", "end-1c") + "\ " + txt_box_fichier_flash.get("1.0", "end-1c")
    nb_q_flash = txt_box_nb_q_flash.get("1.0", "end-1c")
    text = txt_box_ai_flash.get("1.0", "end-1c")


    ia.mode_ia_flash(nb_q_flash,text,acces)


#Programme principal

fenetre_accueil = CTk() #Création de la fenêtre d'accueil --- fenêtre principale qui tournera indéfiniment tant que le programme tourne
fenetre_accueil.title("EDIASSIT")
fenetre_accueil.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_accueil.minsize(1280, 720)
fenetre_accueil.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
set_appearance_mode("dark") #Mode sombre de la bibliothèque customtkinter

#// Elements de la première fenêtre fenêtre_accueil -- fenêtre principale qui tournera en boucle...
#Création d'une boite contenant tous les éléments de la page
boite_1 = CTkFrame(fenetre_accueil, fg_color="transparent")

#Ajout du logo (présent en tant que fichier dans le dossier final) au programme
width=500
height=500 #Dimensions du fichier en .png
logo = PhotoImage(file="logo_EdIAssisst.png").zoom(30).subsample(30)
canvas = Canvas(boite_1, width=width, height=height, bd=0, highlightthickness=0)
canvas.create_image(width/2, height/2, image=logo)
canvas.pack()

#Ajout du bouton "commencer"
btn_commencer = CTkButton(boite_1, text="Commencer", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=creer_fenetre_choix)
#command=creer_fenetre_choix permet de faire un callback de la fonction creer_fenetre_choix à l'appui sur le bouton
btn_commencer.pack()

#Ajout du texte (slogan du projet) en 2 parties pour qu'il preene 2 lignes
txt_slogan = CTkLabel(boite_1, text="Bienvenue sur EdIAssist,", font=("Helvetica", 35), text_color="#1461E0")
txt_slogan.pack(pady=50)
txt_slogan_bis = CTkLabel(boite_1, text="le premier outil de révison qui travaille avec l'IA.", font=("Helvetica", 35), text_color="#1461E0")
txt_slogan_bis.pack()

boite_1.pack(anchor="center") #Placement de la boite au centre

#Ajout du bouton "quitter", sera présent sur chaque page
btn_quitter_1 = CTkButton(fenetre_accueil, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_accueil.destroy)
btn_quitter_1.place(relx=0.85, rely=0.9)
#///


#Création des fenêtres secondaires du programme avec la méthode Toplevel, elles sont directement cachées avec la méthodes withdraw pour qu'elles n'apparaissent qu'en tant voulu

fenetre_choix = CTkToplevel(fenetre_accueil) #On créé une nouvelle fenâtre par-dessus la principale (méthode Topevel)
fenetre_choix.title("EDIASSIT")
fenetre_choix.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_choix.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
fenetre_choix.minsize(1280, 720)
fenetre_choix.withdraw() #Puis on la cache immédiatement

#//Elements de la seconde fenêtre fenetre_choix
#Création d'une boite contenant tous les éléments de la page
boite_2 = CTkFrame(fenetre_choix, fg_color="transparent")

#Ajout de la consigne de cette partie
txt_consigne_choix = CTkLabel(boite_2, text="Veuillez indiquer si vous voulez synthétiser votre cours à travers un QCM ou bien à travers des Flashcards.", font=("Helvetica", 25), text_color="white")
txt_consigne_choix.pack(pady=150) #La méthode pack permet de bien places l'élément dans la fenêtre, et l'instruction pady permet de laiiser un espace avec l'élément du haut

#Ajout du bouton "Flashcard"
btn_flash = CTkButton(boite_2, text="Flashcards", font=("Helvetica", 30), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=creer_fenetre_para_flash)
#command=
btn_flash.pack(pady=50)

#Ajout du bouton "QCM"
btn_qcm = CTkButton(boite_2, text="QCM", font=("Helvetica", 30), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=creer_fenetre_para_qcm)
#command=
btn_qcm.pack(pady=10)

boite_2.pack(anchor="center") #Placement de la boite au centre

#Ajout du traditionnel bouton "quitter", sera présent sur chaque page
btn_quitter_2 = CTkButton(fenetre_choix, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_choix.destroy)
btn_quitter_2.place(relx=0.85, rely=0.9)
#//




fenetre_para_qcm = CTkToplevel(fenetre_accueil) #On créé une nouvelle fenâtre par-dessus la principale (méthode Topevel)
fenetre_para_qcm.title("EDIASSIT")
fenetre_para_qcm.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_para_qcm.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
fenetre_para_qcm.minsize(1280, 720)
fenetre_para_qcm.withdraw() #Puis on la cache immédiatement

#//Elements de la seconde fenêtre fenetre_para_qcm
#Création d'une boite contenant tous les éléments de la page
boite_3 = CTkFrame(fenetre_para_qcm, fg_color="transparent")

#Ajout de la consigne pour le nom du fichier
txt_fichier_qcm= CTkLabel(boite_3, text="Veuillez indiquer le nom de la notion que vous souhaitez travailler.", font=("Helvetica", 25), text_color="white")
txt_fichier_qcm.pack(pady=50)
txt_fichier_qcm_bis = CTkLabel(boite_3, text="Cela sera le nom du fichier dans votre explorateur de fichier.", font=("Helvetica", 25), text_color="white")
txt_fichier_qcm_bis.pack(pady=10)

#Ajout de la zone de texte pour le le nom du fichier
txt_box_fichier_qcm = CTkTextbox(boite_3, width=200, height=20, border_color="#1461E0", border_width=2)
txt_box_fichier_qcm.pack(pady=20)

#Ajout de la consigne pour le nom du dossier
txt_dossier_qcm = CTkLabel(boite_3, text="Veuillez indiquer le nom de la matière que vous souhaitez travailler.", font=("Helvetica", 25), text_color="white")
txt_dossier_qcm.pack(pady=50)
txt_dossier_qcm_bis = CTkLabel(boite_3, text="Cela sera le nom du dossier dans votre explorateur de fichier.", font=("Helvetica", 25), text_color="white")
txt_dossier_qcm_bis.pack(pady=10)

#Ajout de la zone de texte pour le nom du dossier
txt_box_dossier_qcm = CTkTextbox(boite_3, width=200, height=20, border_color="#1461E0", border_width=2)
txt_box_dossier_qcm.pack(pady=20)

#Ajout d'un bouton "valider", pour valider les choix et lancer le QCM
btn_valider_para_qcm = CTkButton(boite_3, text="Valider", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=creer_fenetre_qcm_plus_get_info_explo) #
btn_valider_para_qcm.pack(pady=50)

boite_3.pack(anchor="center") #Placement de la boite au centre

#Ajout du traditionnel bouton "quitter", sera présent sur chaque page
btn_quitter_4 = CTkButton(fenetre_para_qcm, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_para_qcm.destroy)
btn_quitter_4.place(relx=0.85, rely=0.9)
#//



fenetre_para_flash = CTkToplevel(fenetre_accueil) #On créé une nouvelle fenâtre par-dessus la principale (méthode Topevel)
fenetre_para_flash.title("EDIASSIT")
fenetre_para_flash.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_para_flash.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
fenetre_para_flash.minsize(1280, 720)
fenetre_para_flash.withdraw() #Puis on la cache immédiatement

#//Elements de la seconde fenêtre fenetre_para_flash
#Création d'une boite contenant tous les éléments de la page
boite_4 = CTkFrame(fenetre_para_flash, fg_color="transparent")

#Ajout de la consigne pour le nom du dossier
txt_dossier_flash = CTkLabel(boite_4, text="Veuillez indiquer le nom de la matière que vous souhaitez travailler.", font=("Helvetica", 25), text_color="white")
txt_dossier_flash.pack(pady=50)
txt_dossier_flash_bis = CTkLabel(boite_4, text="Cela sera le nom du dossier dans votre explorateur de fichier.", font=("Helvetica", 25), text_color="white")
txt_dossier_flash_bis.pack(pady=10)

#Ajout de la zone de texte pour le le nom du dossier
txt_box_dossier_flash = CTkTextbox(boite_4, width=200, height=20, border_color="#1461E0", border_width=2)
txt_box_dossier_flash.pack(pady=20)

#Ajout de la consigne pour le nom du fichier
txt_fichier_flash= CTkLabel(boite_4, text="Veuillez indiquer le nom de la notion que vous souhaitez travailler.", font=("Helvetica", 25), text_color="white")
txt_fichier_flash.pack(pady=50)
txt_fichier_flash_bis = CTkLabel(boite_4, text="Cela sera le nom du fichier dans votre explorateur de fichier.", font=("Helvetica", 25), text_color="white")
txt_fichier_flash_bis.pack(pady=10)

#Ajout de la zone de texte pour le nom du fichier
txt_box_fichier_flash = CTkTextbox(boite_4, width=200, height=20, border_color="#1461E0", border_width=2)
txt_box_fichier_flash.pack(pady=20)

#Ajout d'un bouton "valider", pour valider les choix et lancer le QCM
btn_valider_para_flash = CTkButton(boite_4, text="Valider", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=creer_fenetre_flash_plus_get_info_explo) #
btn_valider_para_flash.pack(pady=50)

boite_4.pack(anchor="center") #Placement de la boite au centre

#Ajout du traditionnel bouton "quitter", sera présent sur chaque page
btn_quitter_4 = CTkButton(fenetre_para_flash, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_para_flash.destroy)
btn_quitter_4.place(relx=0.85, rely=0.9)
#//




fenetre_qcm = CTkToplevel(fenetre_accueil) #On créé une nouvelle fenâtre par-dessus la principale (méthode Topevel)
fenetre_qcm.title("EDIASSIT")
fenetre_qcm.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_qcm.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
fenetre_qcm.minsize(1280, 720)
fenetre_qcm.withdraw() #Puis on la cache immédiatement

#//Elements de la fenêtre fenetre_qcm
#Création d'une boite contenant tous les éléments de la page
boite_5 = CTkFrame(fenetre_qcm, fg_color="transparent")

#Ajout de la consigne pour le nombre de questions
txt_consigne_nb_q_qcm = CTkLabel(boite_5, text="Veuillez indiquer le nombre de questions vous souhaitez synthétiser (valeur entière entre 2 et 10) :", font=("Helvetica", 25), text_color="white")
txt_consigne_nb_q_qcm.pack(pady=20)

#Ajout de la zone de texte pour le nombre de question
txt_box_nb_q_qcm = CTkTextbox(boite_5, width=50, height=20, border_color="#1461E0", border_width=2)
txt_box_nb_q_qcm.pack(pady=10)

#Ajout de la consigne pour le texte/le cours -- qui sera le prompt pour l'ia
txt_consigne_ai_qcm = CTkLabel(boite_5, text="Veuillez placer ici le texte / le cours qui sert de base à l'IA :", font=("Hevetica", 25), text_color="white")
txt_consigne_ai_qcm.pack(pady=30)

#Ajout de la zone de texte pour l'input de texte
txt_box_ai_qcm = CTkTextbox(boite_5, width=800, height=200, scrollbar_button_color="#1461E0", corner_radius=30, border_color="#1461E0", border_width=2)
txt_box_ai_qcm.pack(pady=10)

#Ajout d'un bouton "lancer", pour valider les choix et lancer le QCM
btn_lancer_qcm = CTkButton(boite_5, text="Lancer", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=get_info_qcm) #
btn_lancer_qcm.pack(pady=30)

boite_5.pack(anchor="center") #Placement de la boite au centre

#Ajout du traditionnel bouton "quitter", sera présent sur chaque page
btn_quitter_5 = CTkButton(fenetre_qcm, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_qcm.destroy)
btn_quitter_5.place(relx=0.85, rely=0.9)
#//




fenetre_flash = CTkToplevel(fenetre_accueil) #On créé une nouvelle fenâtre par-dessus la principale (méthode Topevel)
fenetre_flash.title("EDIASSIT")
fenetre_flash.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
fenetre_flash.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
fenetre_flash.minsize(1280, 720)
fenetre_flash.withdraw() #Puis on la cache immédiatement

#//Elements de la fenêtre fenetre_flash -- très similaire à fenetre_qcm
#Création d'une boite contenant tous les éléments de la page
boite_6 = CTkFrame(fenetre_flash, fg_color="transparent")

#Ajout de la consigne pour le nombre de questions
txt_consigne_nb_q_flash = CTkLabel(boite_6, text="Veuillez indiquer le nombre de questions vous souhaitez synthétiser (valeur entière entre 2 et 10) :", font=("Helvetica", 25), text_color="white")
txt_consigne_nb_q_flash.pack(pady=20)

#Ajout de la zone de texte pour le nombre de question
txt_box_nb_q_flash = CTkTextbox(boite_6, width=50, height=20, border_color="#1461E0", border_width=2)
txt_box_nb_q_flash.pack(pady=10)

#Ajout de la consigne pour le texte/le cours -- qui sera le prompt pour l'ia
txt_consigne_ai_flash = CTkLabel(boite_6, text="Veuillez placer ici le texte / le cours qui sert de base à l'IA :", font=("Hevetica", 25), text_color="white")
txt_consigne_ai_flash.pack(pady=30)

#Ajout de la zone de texte pour l'input de texte
txt_box_ai_flash = CTkTextbox(boite_6, width=800, height=250, scrollbar_button_color="#1461E0", corner_radius=30, border_color="#1461E0", border_width=2)
txt_box_ai_flash.pack(pady=10)

#Ajout d'un bouton "lancer", pour valider les choix et lancer les flashcards
btn_lancer_flash = CTkButton(boite_6, text="Lancer", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=get_info_flash) #
btn_lancer_flash.pack(pady=30)

boite_6.pack(anchor="center") #Placement de la boite au centre

#Ajout du traditionnel bouton "quitter", sera présent sur chaque page
btn_quitter_6 = CTkButton(fenetre_flash, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_flash.destroy)
btn_quitter_6.place(relx=0.85, rely=0.9)
#//



fenetre_accueil.mainloop() #ligne permettant d'afficher la fénêtre créée -- elle tournera en boucle jusqu'à l'arrêt du programme / appui sur "Quitter"

