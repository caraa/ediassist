from tkinter import *
from customtkinter import *






def flashcard(liste_flash):
    """
    fonction permettant l'ouverture des flashcards grace à la liste ci-dessus
    :param liste_flash: list
    :return:
    """

    print("ouverture des flashcards")
    i = 0
    while i < len(liste_flash) :
        print(i)
        question = liste_flash[i]["question"]
        reponse = liste_flash[i]["réponse"]

        fenetre_q_flash = CTk() #Création de la fenêtre de question
        fenetre_q_flash.title("EDIASSIT")
        fenetre_q_flash.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
        fenetre_q_flash.minsize(1280, 720)
        set_appearance_mode("dark") #Mode sombre de la bibliothèque customtkinter



        fenetre_q_flash.deiconify()



        #//Elements d'une fenêtre question pour les flashcards
        #Création d'une boite contenant tous les éléments de la page
        boite_5 = CTkFrame(fenetre_q_flash, fg_color="transparent")

        #Ajout du texte
        txt_q = CTkLabel(boite_5, text="Question", font=("Helvetica", 35), text_color="#1461E0")
        txt_q.pack(pady=100)

        #Ajout de la question
        txt_q_qcm = CTkLabel(boite_5, text=question, font=("Helvetica", 25), text_color="white")
        txt_q_qcm.pack(pady=10)

        #Ajout d'un bouton "afficher réponse", pour valider le choix de la Flashcard
        btn_reponse_flash = CTkButton(fenetre_q_flash, text="Afficher Réponse", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=fenetre_q_flash.withdraw)
        btn_reponse_flash.click = True
        btn_reponse_flash.place(relx=0.42, rely=0.54)


        boite_5.pack(anchor="center") #Placement de la boite au centre

        #Ajout du traditionnel bouton "quitter", sera présent sur chaque page
        btn_quitter_3 = CTkButton(fenetre_q_flash, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_q_flash.destroy)
        btn_quitter_3.place(relx=0.85, rely=0.9)




        fenetre_r_flash = CTkToplevel(fenetre_q_flash)  # Création d'une fenêtre pour la réponse à la question
        fenetre_r_flash.title("EDIASSIT")
        fenetre_r_flash.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
        fenetre_r_flash.minsize(1280, 720)
        set_appearance_mode("dark") #Mode sombre de la bibliothèque customtkinter
        #//Elements d'une fenêtre réponse pour les flashcards
        #Création d'une boite contenant tous les éléments de la page
        fenetre_r_flash.deiconify()

        #Ajout du texte (slogan du projet) en 2 parties pour qu'il preene 2 lignes
        txt_r = CTkLabel(fenetre_r_flash, text="Réponse", font=("Helvetica", 35), text_color="#1461E0")
        txt_r.pack(pady=100)

        #Ajout de la réponse
        txt_r_flash = CTkLabel(fenetre_r_flash, text=reponse, font=("Helvetica", 25), text_color="white")
        txt_r_flash.pack(pady=10)

        #Ajout d'un bouton "j'ai réussi", pour valider le choix du QCM
        btn_reu_flash = CTkButton(fenetre_r_flash, text="J'ai réussi", font=("Helvetica", 25), corner_radius=30, fg_color="#288800", hover_color="#3CCC00", command=fenetre_r_flash.withdraw) #
        btn_reu_flash.place(relx=0.30, rely=0.54)

        #Ajout d'un bouton "je n'ai pas réussi", pour valider le choix du QCM
        btn_pas_reu_flash = CTkButton(fenetre_r_flash, text="Je n'ai pas réussi", font=("Helvetica", 25),corner_radius=30, fg_color="#C70000", hover_color="#BF403C", command=fenetre_r_flash.withdraw) #
        btn_pas_reu_flash.place(relx=0.61, rely=0.54)

        #Ajout du traditionnel bouton "quitter", sera présent sur chaque page
        btn_quitter_3 = CTkButton(fenetre_r_flash, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_r_flash.destroy)
        btn_quitter_3.place(relx=0.85, rely=0.9)



        i= i+1

    fenetre_r_flash.mainloop()
    fenetre_q_flash.mainloop()

