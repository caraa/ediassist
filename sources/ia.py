import requests
import json
#import test_flash_final_2
headers = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNDE5ODM4MDYtMGEwZi00NGYzLWFkYmMtYTU2NTgwOWVmMjhhIiwidHlwZSI6ImFwaV90b2tlbiJ9.JQm4909JhuOsMELHL43iXGeygelTdT7EC2sbAjM6QYM"}
url ="https://api.edenai.run/v2/text/generation"



def mode_ia_flash(n, text, acces):
    """
    le mode_ia_flash sert à creer des les questions et les reponses pour les flashcards
    :param n:
    :param text:
    :param acces:
    :return:
    """
    Liste=[]
    playload = {
        "providers": "mistral",
        "text": "Vous êtes un expert de l'apprentissage. Dans le but de tester les connaissances d'un utilisateur sur un cours, donnez-moi "+n+" question(s) AVEC LA REPONSE COURTE (30 CARACTÈRES MAXIMUM) A CHACUNE D'ELLE (sans écrire 'réponse:') entre guillemets en se basant UNIQUEMENT sur ce texte/cours :"+text ,
        "temperature" : 0.2,
        "max_tokens" : 4000
        }
    reponse = requests.post(url, json=playload, headers=headers)
    result = json.loads(reponse.text)
    print(result)
    result=result["mistral"]["generated_text"]
    try :
        fichier = open(acces+".odt","a", unicode="UTF-8", encoding="UTF-8")
        fichier.write(result)
        fichier.close()
    except:
        pass
    for i in range(int(n)):
        result = result[result.find('"')+1:]
        Liste.append({})
        Liste[i]["question"]=result[0:result.find('"')]
        result = result[result.find('"')+1:]
        result = result[result.find('"')+1:]
        Liste[i]["réponse"]=result[0:result.find('"')]
        result = result[result.find('"')+1:]
    print(Liste)
    test_flash_final_2.flashcard(Liste)

def mode_ia_qcm(n_q, text, acces):
    """
    le mode_ia_flash sert à creer des les questions et les reponses pour les flashcards
    :param n_q:
    :param text:
    :param acces:
    :return:
    """
    Liste=[]
    n_r="3"
    playload = {
        "providers": "mistral",
        "text": "Vous êtes un expert de l'apprentissage. Dans le but de tester les connaissances d'un utilisateur sur un cours, donnez-moi "+n_q+" question(s) AVEC "+n_r+" RÉPONSES (30 CHARACTÈRES) avec une ASTÉRISQUE après la bonne réponse (sans écrire 'réponse:') entre guillemets en se basant UNIQUEMENT sur ce texte/cours :"+text ,
        "temperature" : 0.2,
        "max_tokens" : 4000
        }
    reponse = requests.post(url, json=playload, headers=headers)
    result = json.loads(reponse.text)
    result=result["mistral"]["generated_text"]
    fichier = open(acces + ".odt", "a", encoding="UTF-8")
    fichier.write(acces)
    fichier.close()
    for i in range(int(n_q)):
        result = result[result.find('"')+1:]
        Liste.append({"choix":[]})
        Liste[i]["question"]=result[0:result.find('"')+1]
        result = result[result.find('"')+1:]
        for d in range(3):
            result = result[result.find('"')+1:]
            Liste[i]["choix"]=[result[0:result.find('"')]]+Liste[i]["choix"]
            result = result[result.find('"')+1:]
        for elt in Liste[i]["choix"]:
            if '*' in elt:
                Liste[i]["reponse"]=elt[:elt.find('*')]
                Liste[i]["choix"]=[elt[:elt.find('*')]]+Liste[i]["choix"]
                Liste[i]["choix"].remove(elt)

    fichier = open("info_qcm.py", "a", encoding="UTF-8")
    fichier.write("\nliste_qcm = ")
    fichier.write(str(Liste))
    fichier.close()
    print("Votre variable est chargée, il suffit d'aller dans info_qcm.py pour la coller")
    print("dans le fichier test_qcm_final.py à l'endroit indiqué.")
