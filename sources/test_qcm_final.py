from customtkinter import *



#Fonction pour afficher la question actuelle
def afficher_question():
    """
    fonction permettant d'afficher les question du QCM
    :return:
    """
    #Permet d'obtenir la question actuel depuis la grande liste de départ
    global question_actuelle
    question = liste_qcm[question_actuelle]
    text_question.configure(text=question["question"])

    #Afficher les choix
    choices = question["choix"]
    for i in range(len(choices)):
        choix[i].configure(text=choices[i], state="normal") #Remet l'état du bouton à normal

    #Remet à 0 le texte correct/incorrect et le bouton suivant
    text_retour.configure(text="")
    btn_suivant.configure(state="disabled")


#Fonction pour vérifier si c'est bien la réponse qui est sélectionnée
def verif_rep(choice):
    """
    permets de verifier si le choix pris est la bonne réponse
    :param choice:
    :return:
    """
    #On obtient la question actueele depuis la liste liste_qcm
    question = liste_qcm[question_actuelle]
    proposition_selctionnee = choix[choice].cget("text")

    #On regarde si le choix de l'utilisateur correspond à la réponse
    if proposition_selctionnee == question["reponse"]:
        #On actualise le score
        global score
        score = score + 1
        text_score.configure(text="Score: {}/{}".format(score, len(liste_qcm)))
        text_retour.configure(text="Vous sélectionné la bonne réponse !", text_color="green")
    else:
        text_retour.configure(text="Vous sélectionné la mauvaise réponse !", text_color="red")

    #Désactivation de tous les boutons de choix multiples + activation du bouton "suivant"
    for bouton in choix:
        bouton.configure(state="disabled")
    btn_suivant.configure(state="normal")


#Fonction pour pâsser à la question suivante
def question_suivante():
    """
    fonction permettant de passé à la question suivante
    :return:
    """
    global question_actuelle
    question_actuelle = question_actuelle + 1

    if question_actuelle < len(liste_qcm):
        #Si il y a moins de questions dans la liste de départ que la valeur de question_actuelle, on affiche la question suivante avec la fonction définie plus haut
        afficher_question()
    else:
        #Si
        fenetre_finale_qcm.destroy()


liste_qcm = []



#merci ici de coller la variable liste_qcm




if len(liste_qcm)!=0:
    fichier = open("info_qcm.py","w")
    fichier.write("liste_qcm = []")
    fenetre_finale_qcm = CTk() #Création de la fenêtre d'accueil --- fenêtre principale qui tournera indéfiniment tant que le programme tourne
    fenetre_finale_qcm.title("EDIASSIT")
    fenetre_finale_qcm.geometry("1280x720") #Dimensions de la fenêtre = définition HD 720p
    fenetre_finale_qcm.minsize(1280, 720)
    fenetre_finale_qcm.iconbitmap("logo_app.ico") #Changement de l'icone du logiciel par un fichier en .ico
    set_appearance_mode("dark") #Mode sombre de la bibliothèque customtkinter


    #//Elements de la seconde fenêtre

    #Configuration des boutons

    #Création du texte
    text_question = CTkLabel(fenetre_finale_qcm, font=("Helvetica", 25), text_color="white", anchor="center", wraplength=500, pady=10)
    text_question.pack(pady=10)

    #Création des bouttons de choix
    choix = []
    for i in range(3):
        bouton = CTkButton(fenetre_finale_qcm, font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=lambda i = i: verif_rep(i))
        bouton.pack(pady=5)
        choix.append(bouton)

    #Création du texte du retour correct / incorrect
    text_retour = CTkLabel(fenetre_finale_qcm, font=("Helvetica", 25), anchor="center", pady=10)
    text_retour.pack(pady=10)

    #Initialisation du score
    score = 0

    #Création du texte contenant le score
    text_score = CTkLabel(fenetre_finale_qcm, font=("Helvetica", 25), text="Score: 0/{}".format(len(liste_qcm)), anchor="center", pady=10)
    text_score.pack(pady=10)

    #Création du bouton "suivant"
    btn_suivant = CTkButton(fenetre_finale_qcm, text="Suivant", font=("Helvetica", 25), corner_radius=30, fg_color="#1461E0", hover_color="#1E3D6F", command=question_suivante, state="disabled")
    btn_suivant.pack(pady=10)

    #Ajout du traditionnel bouton "quitter", sera présent sur chaque page
    btn_quitter_6 = CTkButton(fenetre_finale_qcm, text="Quitter", font=("Helvetica", 20), corner_radius=30, fg_color="#E02121", hover_color="#A10D0D",  command=fenetre_finale_qcm.destroy)
    btn_quitter_6.place(relx=0.85, rely=0.9)

    #Initialisation de l'indice de la question actuelle

    question_actuelle = 0

    #Montre la première question
    afficher_question()

    #ligne permettant d'afficher la fénêtre créée -- elle tournera en boucle jusqu'à l'arrêt du programme
    fenetre_finale_qcm.mainloop()

